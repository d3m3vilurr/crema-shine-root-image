## 주의
* 기존 기기테스터 두분이 반벽돌화 된 것으로 추정됩니다. 테스터셨던 분들은 이 루팅 절차를 진행하지 말아주세요.
* 이 루팅 과정으로 생기는 문제는 전적으로 자신의 책임입니다. 루팅 과정으로 생기는 문제로 이후에 A/S에 제한을 받을 수 있으며, 디폴트 기능에 차이가 생길 수 있습니다.

## Change History
* 1.5.50
    * 1.5.50 으로 올림
    * SmartLauncher 2.5.1d 로 올림
* 1.5.30-1
    * 새 `boot.img` 사용 시 home 버튼으로 리프레시가 안되는 문제 수정
    * 가로모드에서 home 버튼에 반응이 없던 문제 수정
* 1.5.30
    * 1.5.30으로 올림
    * 기본앱에 [ADB Control][5] 추가
* 1.5.10
    * 1.5.10으로 올림
* 1.4.40
    * 1.4.40으로 올림
* 1.4.00
    * 1.4.00으로 올림
    * RefreshPie 1.1.8로 올림
* 1.3.10
    * 1.3.10으로 올림
    * 기본앱에 [RefreshPie][3] 추가
* 1.2.10
    * 1.2.10으로 올림
    * 버전올림으로 인해 크래마 리프레시 기능 고쳐짐
* 1.0.10-2
    * 기본 메뉴 앱(`MelonWizard.apk`) 제거
    * 런쳐와 화면 리프레시를 위한 헬퍼 앱 추가(`Wizard.apk`, `WizardChain.apk`)
* 1.0.10-1
    * 기본앱 중 구동에 문제가 되던 `Perfect Viewer`, `Orion Viewer` 를 제거했습니다.
* 1.0.10
    * 기본앱 변경
        - 런쳐: `SmartLauncher`
        - 그외: `Seeder`, `NT Hidden Settings`, `Perfect Viewer`, `Orion Viewer`
    * zip 파일명들 변경
        - system.stock.zip
        - system.add-default-apps.zip
        - system.add-default-apps-and-gapps.zip
        - system.add-default-apps-and-gapps-and-active-bar.zip
    * 순정 update.zip 파일 추가

## 알려진 문제
* 1.5.50
    * 화면이 간헐적으로 뿌옇게 흐려지는 현상 발생
* 1.3.10
    * `SmartLauncher` 에 간헐적으로 신규 추가된 어플이 보이지 않음
    * 홈버튼으로 기기 깨우기 기능이 정상적으로 동작하지 않음
* 1.2.10
    * 홈버튼 더블클릭을 약 0.5초 정도의 딜레이를 주면서 누르지 않으면, 홈으로 이동되지 않음
* 1.0.10-2
    * 간혹 홈버튼 원클릭 시 홈으로 계속 돌아가는 버그가 있습니다. (단순하게는 리부팅 등으로 해결될 수 있습니다.)
    * 기본 크레마 앱에서 동작하던 N장 이후 리프레시 기능이 동작하지 않습니다.
    * 리프레시 앱이 여러번 불리는 것처럼 표시되는 문제가 있습니다.
* 1.0.10
    * 일부 어플 구동 실패

## 설치순서
1. 안드로이드 개발툴킷에 포함된 [fastboot][1] 를 설치합니다. 필요 시 [USB 장치드라이버][2]를 설치하셔야 합니다.
2. 롬 파일을 다운로드 받아 압축을 해제합니다..

    * 1.0.00 - http://goo.gl/UrcbhY
    * 1.0.10 - http://goo.gl/gh4hNi
    * 1.0.10-1 - http://goo.gl/b2ng9r
    * 1.0.10-2 - http://goo.gl/2vkxnp
    * 1.2.10 - http://goo.gl/bJdBNv
    * 1.3.10 - http://goo.gl/m5hSp5
    * 1.4.00 - http://goo.gl/DuAHdJ
    * 1.4.40 - http://goo.gl/b4PXoI
    * 1.5.10 - http://goo.gl/GRzdq6

    또는 [패치셋](http://goo.gl/rXGjQ0) 을 다운로드 받아 기존 파일을 [xdelta3][4]를 이용 패치합니다.

    파일명 별로 추가되는 어플리케이션에 차이가 존재합니다.

    * system.stock.zip
    * system.add-default-apps.zip - su 명령과 기타 추가 어플
    * system.add-default-apps-and-gapps.zip - google play system
    * system.add-default-apps-and-gapps.and-active-bar.zip - 하단 소프트웨어 바 활성화

3. 크레마 샤인을 완전 종료하신 후(18초정도), 홈키와 전원 버튼을 같이 누른채로 2, 3초 정도 대기하시기 바랍니다. 화면 변화는 없이 기기가 계속 빨간불이 들어와 있으면 됩니다.
4. fastboot 모드로 정상적으로 들어갔는지 커맨드로 확인합니다.

        $ fastboot devices
        12345 fastboot

5. fastboot를 이용해 이미지를 기기에 설치합니다.

        $ fastboot flash system system.add-default-apps-and-gapps-and-active-bar.img

6. 기기를 재시작합니다.

        $ fastboot reboot

## ADB
1. ADB Control 에서 ADB 기능을 활성화 합니다.
2. `~/.android/adb_usb.ini` 파일 마지막에 `0x1f85`를 추가합니다.
3. `adb kill-server && adb devices` 을 하여 디바이스가 잡히는지 확인합니다.

[1]: https://texasice-cream-sandwich-mirror.googlecode.com/files/adb_fastboot.zip
[2]: https://dl-ssl.google.com/android/repository/latest_usb_driver_windows.zip
[3]: https://github.com/ztoday21/refreshPie
[4]: http://xdelta.org/
[5]: https://play.google.com/store/apps/details?id=com.pixplicity.adb
