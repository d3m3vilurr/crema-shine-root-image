#!/usr/bin/env bash

PREV=$1
PREV_HASH=$2
NEXT=$3
NEXT_HASH=$4

if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ] || [ -z "$4" ]; then
    echo "$0 <prev tag> <hash> <target tag> <hash>"
    exit 1;
fi

rm -rf ${PREV}_to_${NEXT}
mkdir ${PREV}_to_${NEXT}
cd ${PREV}_to_${NEXT}

for f in system.stock system.add-default-apps system.add-default-apps-and-gapps system.add-default-apps-and-gapps-and-active-bar
do
    echo GET FILES ${f}.zip.${PREV} ${f}.zip.${NEXT}
    git show ${PREV_HASH}:../../images/${f}.zip > ${f}.zip.${PREV}
    git show ${NEXT_HASH}:../../images/${f}.zip > ${f}.zip.${NEXT}
    unzip ${f}.zip.${PREV}
    mv ${f}.img ${f}.img.${PREV}
    unzip ${f}.zip.${NEXT}
    mv ${f}.img ${f}.img.${NEXT}
    echo "=> gen delta"
    xdelta3 -es ${f}.img.${PREV} ${f}.img.${NEXT} ${f}.img.xdelta3
done
rm -r *.zip.*
md5sum * > md5.checksum
rm -r *.img.${PREV} *.img.${NEXT}
