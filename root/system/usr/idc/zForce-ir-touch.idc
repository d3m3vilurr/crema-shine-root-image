touch.deviceType = touchScreen
touch.orientationAware = 1

# Touch Size
touch.size.scale = 10
touch.size.bias = 0
touch.size.isSummed = 0
touch.touchSize.calibration = pressure

# Pressure
touch.pressure.calibration = amplitude
touch.pressure.source = default
touch.pressure.scale = 0.01

# Size
touch.size.calibration = diameter

# Orientation
touch.orientation.calibration = none
