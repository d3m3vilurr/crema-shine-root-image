## CAUTION

DISCLAIMER: I AM NOT RESPONSIBLE IF YOU BRICK / RUIN YOUR DEVICE IN ANY WAY.
BASIC COMPUTER SKILLS REQUIRED

BETA TESTER's Device incorrect this rooting image. If update this rom into beta device, may cause BRICK Device.

## Change History
* 1.5.50
    * Update base system 1.5.50
    * Update SmartLauncher 2.5.1d
* 1.5.30-1
    * Fix home button refresh on using new `boot.img`
    * Fix no action home button of protrait orientation mode
* 1.5.30
    * Update base system 1.5.30
    * Add [ADB Control][5] into basic app
* 1.5.10
    * Update base system 1.5.10
* 1.4.40
    * Update base system 1.4.40
* 1.4.00
    * Update base system 1.4.00
    * Update RefreshPie 1.1.8
* 1.3.10
    * Update base system 1.3.10
    * Add [RefreshPie][3] into basic app
* 1.2.10
    * Update base system 1.2.10
    * Fixed refresh feature
* 1.0.10-2
    * Remove base menu application(`MelonWizard.apk`)
    * Add helper app(`Wizard.apk`, `WizardChain.apk`)
        * Home oneclick - refresh screen
        * Home slowly double click - Go to Home launcher
* 1.0.10-1
    * Remove problem basic apps `Perfect Viewer`, `Orion Viewer`
* 1.0.10
    * Add basic app
        - Launcher: `SmartLauncher`
        - ETC: `Seeder`, `NT Hidden Settings`, `Perfect Viewer`, `Orion Viewer`
    * Change zip filenames
        - system.stock.zip
        - system.add-default-apps.zip
        - system.add-default-apps-and-gapps.zip
        - system.add-default-apps-and-gapps-and-active-bar.zip
    * Add stock `update.zip`

## Known Issues
* 1.5.50
    * Sometimes scereen become dim
* 1.3.10
    * Sometimes never show new system application in `SmartLauncher`
    * Not work `home button click awake on sleep mode`
* 1.2.10
    * Go to Home launcher, need 0.5s delay of home button double click
* 1.0.10-2
    * Sometimes home one click, return Home launcher; need device reboot
    * Not work Nth touch auto refresh
    * Look like unclear refresh screen.
* 1.0.10
    * Some basic application broken

## Install steps
1. Install [fastboot][1] and [USB Device Driver][2](windows case)
2. Download rom and unzip

    * 1.0.00 - http://goo.gl/UrcbhY
    * 1.0.10 - http://goo.gl/gh4hNi
    * 1.0.10-1 - http://goo.gl/b2ng9r
    * 1.0.10-2 - http://goo.gl/2vkxnp
    * 1.2.10 - http://goo.gl/bJdBNv
    * 1.3.10 - http://goo.gl/m5hSp5
    * 1.4.00 - http://goo.gl/DuAHdJ
    * 1.4.40 - http://goo.gl/b4PXoI
    * 1.5.10 - http://goo.gl/GRzdq6

    Or Download [Patch Set](http://goo.gl/rXGjQ0) and patch previous file using [xdelta3][4].

    Each file has some difference additional application

    * system.stock.zip
    * system.add-default-apps.zip - add su binary and etc
    * system.add-default-apps-and-gapps.zip - add google play system
    * system.add-default-apps-and-gapps.and-active-bar.zip - active software bottom bar

3. Fully shutdown crema shine(hold power more than 18 sec) and boot on Home + Power Hold(2~3 sec). Not changed screen, and on red LED
4. Check fastboot mode on.

        $ fastboot devices
        12345 fastboot

5. Rom write using fastboot

        $ fastboot flash system system.add-default-apps-and-gapps-and-active-bar.img

6. Reboot device

        $ fastboot reboot

## ADB
1. Enable ADB of ADB Control APP.
2. Open `~/.android/adb_usb.ini`, append `0x1f85` to last line.
3. `adb kill-server && adb devices` than check your device.

[1]: https://texasice-cream-sandwich-mirror.googlecode.com/files/adb_fastboot.zip
[2]: https://dl-ssl.google.com/android/repository/latest_usb_driver_windows.zip
[3]: https://github.com/ztoday21/refreshPie
[4]: http://xdelta.org/
[5]: https://play.google.com/store/apps/details?id=com.pixplicity.adb
