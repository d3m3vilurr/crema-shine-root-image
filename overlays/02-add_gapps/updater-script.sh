#!/usr/bin/env bash
echo "Delete old image..."
rm *.img
echo "Copy lastest 01 step image..."
unzip ../../images/system.add-default-apps.zip
mv system.add-default-apps.img system.add-default-apps-and-gapps.img
echo "Mounting system..."
mkdir system-target
sudo mount -t ext4 -o loop system.add-default-apps-and-gapps.img ./system-target
echo "Copying files..."
sudo cp -r system/* system-target
echo "Fixing permissions..."
sync
df | grep system-target
echo "Unmounting system..."
sudo umount ./system-target
rm -rf system-target
echo "Installation complete!"
echo "Zip image..."
zip system.add-default-apps-and-gapps.zip system.add-default-apps-and-gapps.img
mv system.add-default-apps-and-gapps.zip ../../images
