#!/usr/bin/env bash
echo "Fix portrait mode home key"
rm -rf framework
mkdir framework
cp -rf ../../root/system/framework/android.policy.* framework/
cd framework
java -Xmx512m -jar ../baksmali.jar -x "android.policy.odex" -a15 -d /system/framework/
cd ..
python fix-portrait-home-key.py
cd framework
java -Xmx512M -jar ../smali.jar out/ -a15 -o classes.dex

mkdir -p working/build
cp "classes.dex" "working/build/"
cp "android.policy.jar" "working/"

cd working
7za x -o"build" android.policy.jar >> LOG-JAR.txt
cd build
7za a -tzip Done.jar * >> LOG-JAR.txt #-mx${compression} >> LOG-JAR.txt
cd ..
cd ..
rm -rf ../mod-android.policy.jar
cp -f "working/build/Done.jar" "../mod-android.policy.jar"
cd ..
rm -rf framework
adb push mod-android.policy.jar /sdcard/mod-android.policy.jar
adb shell rm /sdcard/android.policy.odex.new
adb shell /data/local/dexopt-wrapper /sdcard/mod-android.policy.jar /sdcard/android.policy.odex.new "/system/framework/core.jar:/system/framework/core-junit.jar:/system/framework/bouncycastle.jar:/system/framework/ext.jar:/system/framework/framework.jar:/system/framework/services.jar:/system/framework/apache-xml.jar:/system/framework/filterfw.jar"
adb shell /data/local/busybox dd if=/system/framework/android.policy.odex of=/sdcard/android.policy.odex.new bs=1 count=20 skip=52 seek=52 conv=notrunc
adb pull /sdcard/android.policy.odex.new
mkdir -p system/framework
rm -rf system/framework/android.policy.odex
cp -f android.policy.odex.new system/framework/android.policy.odex

echo "Delete old image..."
rm *.img
echo "Copy lastest stock image..."
unzip ../../images/system.stock.zip
mv system.stock.img system.add-default-apps.img
echo "Mounting system..."
mkdir system-target
sudo mount -t ext4 -o loop system.add-default-apps.img ./system-target
echo "Deleting old files..."
sudo rm system-target/bin/su system-target/xbin/su \
        system-target/app/Superuser.apk
echo "Deleting unused files..."
sudo rm system-target/app/MelonWizard.*
echo "Copying files..."
sudo cp -r system/* system-target
echo "Fixing permissions..."
sudo chmod 06755 system-target/bin/su
echo "Symlinking..."
sudo ln -sf /system/bin/su system-target/xbin/su
sync
df | grep system-target
echo "Unmounting system..."
sudo umount ./system-target
rm -rf system-target
echo "Installation complete!"
echo "Zip image..."
zip system.add-default-apps.zip system.add-default-apps.img
mv system.add-default-apps.zip ../../images
