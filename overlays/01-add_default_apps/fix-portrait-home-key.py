#!/usr/bin/env python

TARGET = './framework/out/com/android/internal/policy/impl/PhoneWindowManager.smali'

LINES = []
with open(TARGET) as f:
    SECTION = None
    for i, x in enumerate(f.xreadlines()):
        x = x.rstrip()
        if '__DEBUG__' in x:
            print 'IN TARGET SECTION'
            SECTION = 'DEBUG'
        if not SECTION:
            LINES.append(x)
            continue
        if x[-3:] == '0x1':
            print i, x
            x = x[:-3] + '0x5'
            print '\t=>', x
        if 'if-ne' in x:
            print i, x
            index = x.index('if-ne')
            x = x[:index] + 'if-eq' + x[index+5:]
            print '\t=>', x
            SECTION = None
            print 'SECTION END'
        LINES.append(x)
with open(TARGET, 'w') as f:
    f.write('\n'.join(LINES))
