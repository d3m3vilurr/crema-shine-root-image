#!/usr/bin/env bash
echo "Delete old image..."
rm *.img
echo "Copy lastest 02 step image..."
unzip ../../images/system.add-default-apps-and-gapps.zip
mv system.add-default-apps-and-gapps.img \
   system.add-default-apps-and-gapps-and-active-bar.img
echo "Mounting system..."
mkdir system-target
sudo mount -t ext4 -o loop \
     system.add-default-apps-and-gapps-and-active-bar.img ./system-target
echo "Copying source files..."
mkdir tmp
cp system-target/framework/framework-res.apk tmp
cd tmp
echo "Resize height..."
java -jar ../apktool.jar d framework-res.apk
perl -pi -e \
     's/navigation_bar_height">48.0dip/navigation_bar_height">37.0dip/g' \
     framework-res/res/values/dimens.xml
perl -pi -e 's/status_bar_height">25.0dip/status_bar_height">22.0dip/g' \
     framework-res/res/values/dimens.xml
perl -pi -e \
     's/config_showNavigationBar">false/config_showNavigationBar">true/g' \
     framework-res/res/values/bools.xml
echo "Rebuild..."
java -jar ../apktool.jar b framework-res
rm framework-res/build/apk/AndroidManifest.xml
cd framework-res/build/apk
jar -uf ../../../framework-res.apk *
cd ../../..
echo "Copying files..."
sudo cp framework-res.apk ../system-target/framework/
cd ..
sync
echo "Unmounting system..."
sudo umount ./system-target
rm -rf system-target tmp
echo "Installation complete!"
echo "Zip image..."
zip system.add-default-apps-and-gapps-and-active-bar.zip \
    system.add-default-apps-and-gapps-and-active-bar.img
mv system.add-default-apps-and-gapps-and-active-bar.zip ../../images
