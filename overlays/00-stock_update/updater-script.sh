#!/usr/bin/env bash
symlink() {
    sudo rm -rf $2
    sudo ln -sf $1 $2
}
set_perm_recursive() {
    sudo chown -R $1:$2 $5
    sudo find $5 -type d | xargs sudo chmod $3
    sudo find $5 -type f | xargs sudo chmod $4
}
set_perm() {
    sudo chown $1:$2 $4
    sudo chmod $3 $4
}

echo "Delete old image..."
rm *.img
echo "Copy lastest stock image..."
unzip ../../images/system.stock.zip
sync

echo "installing firmware..."
echo "=> pass :)"
echo "installing system..."
echo "clean up system..."
#sudo rm -rf system-target/*
yes | sudo mkfs.ext4 system.stock.img
sync
echo "Mounting system..."
mkdir system-target
sudo mount -t ext4 -o loop system.stock.img ./system-target
sync
echo "extract package..."
sudo cp -r ../../root/system/* system-target/
sync
echo "setting symlinks and permissions..."
symlink "toolbox" "system-target/bin/cat"
symlink "toolbox" "system-target/bin/chmod"
symlink "toolbox" "system-target/bin/chown"
symlink "toolbox" "system-target/bin/mkdir"
symlink "toolbox" "system-target/bin/mount"
symlink "toolbox" "system-target/bin/mv"
symlink "toolbox" "system-target/bin/rm"
symlink "toolbox" "system-target/bin/umount"
symlink "toolbox" "system-target/bin/lsmod"
symlink "toolbox" "system-target/bin/cmp"
symlink "toolbox" "system-target/bin/date"
symlink "toolbox" "system-target/bin/dd"
symlink "toolbox" "system-target/bin/df"
symlink "toolbox" "system-target/bin/dmesg"
symlink "toolbox" "system-target/bin/getevent"
symlink "toolbox" "system-target/bin/getprop"
symlink "toolbox" "system-target/bin/hd"
symlink "toolbox" "system-target/bin/ls"
symlink "toolbox" "system-target/bin/id"
symlink "toolbox" "system-target/bin/ifconfig"
symlink "toolbox" "system-target/bin/iftop"
symlink "toolbox" "system-target/bin/insmod"
symlink "toolbox" "system-target/bin/ioctl"
symlink "toolbox" "system-target/bin/ionice"
symlink "toolbox" "system-target/bin/kill"
symlink "toolbox" "system-target/bin/ln"
symlink "toolbox" "system-target/bin/log"
symlink "toolbox" "system-target/bin/nandread"
symlink "toolbox" "system-target/bin/netstat"
symlink "toolbox" "system-target/bin/lsof"
symlink "toolbox" "system-target/bin/newfs_msdos"
symlink "toolbox" "system-target/bin/notify"
symlink "toolbox" "system-target/bin/printenv"
symlink "toolbox" "system-target/bin/ps"
symlink "toolbox" "system-target/bin/reboot"
symlink "toolbox" "system-target/bin/renice"
symlink "toolbox" "system-target/bin/rmdir"
symlink "toolbox" "system-target/bin/rmmod"
symlink "toolbox" "system-target/bin/route"
symlink "toolbox" "system-target/bin/schedtop"
symlink "toolbox" "system-target/bin/sendevent"
symlink "toolbox" "system-target/bin/setconsole"
symlink "toolbox" "system-target/bin/setprop"
symlink "toolbox" "system-target/bin/sleep"
symlink "toolbox" "system-target/bin/smd"
symlink "toolbox" "system-target/bin/start"
symlink "toolbox" "system-target/bin/stop"
symlink "toolbox" "system-target/bin/sync"
symlink "toolbox" "system-target/bin/top"
symlink "toolbox" "system-target/bin/uptime"
symlink "toolbox" "system-target/bin/vmstat"
symlink "toolbox" "system-target/bin/watchprops"
symlink "toolbox" "system-target/bin/wipe"
sync
set_perm_recursive 0 0 0755 0644 "system-target"
set_perm_recursive 0 0 0755 0644 "system-target/app"
set_perm_recursive 0 2000 0755 0755 "system-target/bin"
sync
set_perm 0 3003 02750 "system-target/bin/netcfg"
set_perm 0 3004 02755 "system-target/bin/ping"
set_perm 0 2000 06755 "system-target/bin/reboot"
set_perm 0 2000 06750 "system-target/bin/run-as"
sync
set_perm_recursive 0 0 0755 0644 "system-target/etc"
sync
set_perm 1002 1002 0440 "system-target/etc/dbus.conf"
set_perm 0 2000 0550 "system-target/etc/init.goldfish.sh"
set_perm 0 2000 0550 "system-target/etc/init.gprs-pppd"
sync
set_perm_recursive 1002 1002 0755 0440 "system-target/etc/bluetooth"
sync
set_perm 0 0 0755 "system-target/etc/bluetooth"
set_perm 3002 3002 0444 "system-target/etc/bluetooth/blacklist.conf"
set_perm 1000 1000 0640 "system-target/etc/bluetooth/auto_pairing.conf"
set_perm 1014 2000 0550 "system-target/etc/dhcpcd/dhcpcd-run-hooks"
sync
set_perm_recursive 0 0 0755 0555 "system-target/etc/ppp"
set_perm_recursive 0 0 0755 0644 "system-target/fonts"
set_perm_recursive 0 0 0755 0644 "system-target/framework"
set_perm_recursive 0 0 0755 0644 "system-target/lib"
set_perm_recursive 0 0 0755 0644 "system-target/usr"
set_perm_recursive 0 2000 0755 0755 "system-target/vendor"
set_perm_recursive 0 2000 0755 0755 "system-target/xbin"
sync
set_perm 0 0 06755 "system-target/xbin/tcpdump"
set_perm 0 0 06755 "system-target/xbin/su"
set_perm 0 0 06755 "system-target/xbin/procrank"
set_perm 0 0 06755 "system-target/xbin/procmem"
set_perm 0 0 06755 "system-target/xbin/librank"
echo "unmount system..."
sudo umount ./system-target
sudo rm -rf system-target
echo "Installation complete!"
echo "Zip image..."
zip system.stock.zip system.stock.img
mv system.stock.zip ../../images
